//
//  ContentView.swift
//  Project2GuessTheFlag
//
//  Created by AJAY on 08/07/20.
//  Copyright © 2020 Individual. All rights reserved.
//

import Foundation
import SwiftUI

struct ContentView: View {
    
   @State private var countries = ["Estonia", "France", "Germany", "Ireland", "Italy", "Nigeria", "Poland", "Russia", "Spain", "UK", "US"].shuffled()
    
    //.shuffled() = > In order for this game to be fun, we need to randomize the order in which flags are shown, trigger an alert telling them whether they were right or wrong whenever they tap a flag, then reshuffle the flags.
    
   @State private var correctAnswer = Int.random(in: 0...2)
    
    //The Int.random(in:) method automatically picks a random number, which is perfect here – we’ll be using that to decide which country flag should be tapped.
    
    @State private var showingScore = false
    @State private var scoreTitle = ""
    @State private var userScore = 0
    
    var body: some View {
        
        
        /* for now let’s put in a blue background color to make the flags easier to see. Because this means putting something behind our outer VStack, we need to use a ZStack as well. Yes, we’ll have a VStack inside another VStack inside a ZStack, and that is perfectly normal.*/
        ZStack{
            
           // Color.blue.edgesIgnoringSafeArea(.all)
            LinearGradient(gradient: Gradient(colors: [.black,.blue]), startPoint: .top, endPoint: .bottom).edgesIgnoringSafeArea(.all)
            
            VStack(spacing: 30) {
                VStack {
                    Text("Tap the flag of").foregroundColor(.white)
                    Text(countries[correctAnswer]).foregroundColor(.white).font(.largeTitle).fontWeight(.black)
                    
                    //the most prominent piece of text on the screen. We can do that with the font() modifier, which lets us select from one of the built-in font sizes on iOS, but we can add fontWeight() to it to make the text extra bold.
                    
                }
                //Below there we want to have our tappable flag buttons, and while we could just add
                
                
             /*   There are four built-in shapes in Swift: rectangle, rounded rectangle, circle, and capsule. We’ll be using capsule here: it ensures the corners of the shortest edges are fully rounded, while the longest edges remain straight – it looks great for buttons. Making our image capsule shaped is as easy as adding the .clipShape(Capsule()) modifier, like this:*/
                
                ForEach(0 ..< 3){number in
                    
                    Button(action: {
                        self.flagTapped(number)
                    }) {
                        Image(self.countries[number])
                        .renderingMode(.original)
                        .clipShape(Capsule())
                        .overlay(Capsule().stroke(Color.black, lineWidth: 1)) //create Border
                        .shadow(color: .black, radius: 2)
                    }
                    
                }
                
                Text("Your Current Scrore : \(userScore)").foregroundColor(.white).font(.largeTitle).fontWeight(.black)
                
                Spacer()
            }
        }.alert(isPresented: $showingScore){
            Alert(title: Text(scoreTitle), message: Text("Your score is \(userScore)"), dismissButton:.default(Text("Continue")){
                self.askQuestion()
                })
        }
        
    }
    
    
    func flagTapped(_ number: Int) {
        if number == correctAnswer {
            scoreTitle = "Correct! That’s the flag of \(countries[number])"
            userScore = userScore + 1
        } else {
            scoreTitle = "Wrong! That’s the flag of \(countries[number])"
        }

        showingScore = true
    }
    
    func askQuestion() {
        countries.shuffle()
        correctAnswer = Int.random(in: 0...2)
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}

